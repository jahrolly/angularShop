import {Injectable} from '@angular/core';
import {CanActivate, Router} from '@angular/router';
import {AuthorizationService} from "../services/authorization.service";


@Injectable()
export class  AuthorizationGuard implements CanActivate {
  constructor(private authorizationService: AuthorizationService,
              private router: Router) {

  }Q

  canActivate() {
    if (this.authorizationService.isLoggedIn()) {
      return true;
    } else {
      this.router.navigate(['/login']);
      return false;
    }
  }
}
