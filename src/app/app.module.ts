import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import {AppComponent} from './app.component';
import {ProductsPanelComponent} from './components/products-panel/products-panel.component';
import {ProductComponent} from './components/products-panel/product/product.component';
import {FiltersComponent} from './components/products-panel/filters/filters.component';
import {ShoppingCartComponent} from './components/shopping-cart/shopping-cart.component';
import {CategoryFilterComponent} from './components/products-panel/filters/category-filter/category-filter.component';
import {PaginationComponent} from './components/products-panel/pagination/pagination.component';
import {ShoppingCartService} from "./services/shopping-cart.service";
import {ProductsService} from "./services/products.service";
import {CheckoutComponent} from './components/checkout/checkout.component';
import {HttpClientModule} from "@angular/common/http";
import {PriceFilterComponent} from './components/products-panel/filters/price-filter/price-filter.component';
import {NameFilterComponent} from './components/products-panel/filters/name-filter/name-filter.component';
import {urlsService} from "./services/urls.service";
import {AdminPanelComponent} from './admin-panel/admin-panel.component';
import {RouterModule, Routes} from "@angular/router";
import {HomeComponentComponent} from './home-component/home-component.component';
import {OrdersComponent} from './admin-panel/orders/orders.component';
import {OrderComponent} from './admin-panel/orders/order/order.component';
import {OrdersService} from "./services/orders.service";
import {ProductComponentShort} from "./admin-panel/orders/order/product/product.component";
import {EditProductsPanelComponent} from './admin-panel/edit-products-panel/edit-products-panel.component';
import {EditableProductComponent} from './admin-panel/edit-products-panel/editable-product/editable-product.component';
import {LoginComponent} from './components/login/login.component';
import {AuthorizationGuard} from "./guards/AuthorizationGuard";
import {AuthorizationService} from "./services/authorization.service";
import {NewProductComponent} from './admin-panel/edit-products-panel/new-product/new-product.component';
import {UserloginComponent} from './components/userlogin/userlogin.component';
import {LoginHeaderComponent} from './components/login-header/login-header.component';
import {ServerNotificationService} from "./services/server-notification.service";
import { DiscountComponentComponent } from './admin-panel/discount-component/discount-component.component';
import { DiscountableProductComponent } from './admin-panel/discount-component/discountable-product/discountable-product.component';
import { Ng4FilesModule } from 'angular4-files-upload';
import {FileUploadModule} from "ng2-file-upload";
import { SignupComponent } from './components/login/signup/signup.component';
import {UserService} from "./services/user.service";
import { OrdersHistoryComponent } from './components/orders-history/orders-history.component';
import { SingleOrderComponent } from './components/single-order/single-order.component';
import { ModalInfoComponent } from './components/products-panel/product/modal-info/modal-info.component';
import {NgbModal, NgbModule} from "@ng-bootstrap/ng-bootstrap";
import { CarouselModule } from 'angular4-carousel';

const appRoutes: Routes = [
  {path: '', component: HomeComponentComponent},
  {path: 'login', component: LoginComponent},
  {path: 'admin', component: AdminPanelComponent, canActivate: [AuthorizationGuard]}
];

@NgModule({
  declarations: [
    AppComponent,
    ProductsPanelComponent,
    ProductComponent,
    FiltersComponent,
    ShoppingCartComponent,
    CategoryFilterComponent,
    PaginationComponent,
    CheckoutComponent,
    PriceFilterComponent,
    NameFilterComponent,
    AdminPanelComponent,
    HomeComponentComponent,
    OrdersComponent,
    OrderComponent,
    ProductComponentShort,
    EditProductsPanelComponent,
    EditableProductComponent,
    LoginComponent,
    NewProductComponent,
    UserloginComponent,
    LoginHeaderComponent,
    DiscountComponentComponent,
    DiscountableProductComponent,
    SignupComponent,
    OrdersHistoryComponent,
    SingleOrderComponent,
    ModalInfoComponent
  ],
  imports: [
    RouterModule.forRoot(appRoutes),
    BrowserModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    FileUploadModule,
    NgbModule.forRoot(),
    CarouselModule
  ],
  entryComponents: [
    ModalInfoComponent
  ],
  providers: [ShoppingCartService, ProductsService, urlsService, OrdersService,
    AuthorizationGuard, AuthorizationService, ServerNotificationService, UserService, NgbModal],
  bootstrap: [AppComponent]
})
export class AppModule {
}
