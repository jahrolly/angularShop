import {Order} from "./Order";

export class RawUser {
  constructor(public username: string,
              public password: string) {
  }
}

export class User {
  public ordersIds : Order[] = [];
  constructor(public name: string,
              public surname: string,
              public username : string,
              public password : string,
              public email: string,
              public address: string,
              public phoneNumber: number) {}
}
