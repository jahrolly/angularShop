import {Product} from "./Product";

export class Order {
  public isCompleted: boolean = false;
  public _id : string = "";
  constructor(public name: string,
              public surname: string,
              public email: string,
              public address: string,
              public phoneNumber: number) {
    this.orderedProductsIds = []
  }
  public orderedProductsIds : Array<String>;
}
