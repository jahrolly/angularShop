export class Product {
  public images : string[] = [];
  constructor(public _id: string,
              public name: string,
              public price: number,
              public quantity: number,
              public category: string,
              public description: string,
              public DiscountedAmount: Number)
  {}
}


export class ProductWrapper {
  constructor(public product : Product) {}
  isNew: boolean = false;
}
