export class Discount
{
  constructor(public ids : Array<string>, public durationInMinutes : number, public discountPercent : number) {}
}
