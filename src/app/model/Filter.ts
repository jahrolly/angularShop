export class Filter {
  constructor(public partOfName: string,
              public category: string,
              public priceFrom: number,
              public priceTo: number) {}
}
