import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductComponentShort } from './product.component';

describe('ProductComponentShort', () => {
  let component: ProductComponentShort;
  let fixture: ComponentFixture<ProductComponentShort>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductComponentShort ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductComponentShort);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
