import {Component, Input, OnInit} from '@angular/core';
import {Product} from "../../../../model/Product";

@Component({
  selector: 'app-product-short',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponentShort{
  @Input() product  : Product;
  @Input() index  : number;
}
