import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Order} from "../../../model/Order";
import {Product} from "../../../model/Product";
import {ProductsService} from "../../../services/products.service";
import { CurrencyPipe } from '@angular/common';
import {OrdersService} from "../../../services/orders.service";

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.css']
})
export class OrderComponent implements OnInit {
  @Input() order: Order;
  @Output() notify : EventEmitter<any> = new EventEmitter<any>();
  products : Array<Product>;
  showDetails: boolean = false;
  totalAmount: Number;

  constructor(private productsService: ProductsService, private ordersService: OrdersService) {
  }

  ngOnInit(): void {
    this.getPurchasedProducts();
  }

  getPurchasedProducts() {
    this.productsService.getAllProducts().subscribe(products => {
      this.products = products.filter(product => this.order.orderedProductsIds.includes(product._id));
      this.totalAmount = this.getTotalAmount();
    });
  }

  getTotalAmount() {
    return (this.products==null) || !this.products.length ? 0 : this.products.map(product => {
      return product.price
    }).reduce((a, b) => {
      return a + b;
    })
  }

  markAsCompleted() {
    this.ordersService.markAsCompleted(this.order).subscribe(() => {
      this.notify.emit();
    });
  }
}
