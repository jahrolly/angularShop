import { Component, OnInit } from '@angular/core';
import {OrdersService} from "../../services/orders.service";
import {Order} from "../../model/Order";

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.css']
})
export class OrdersComponent implements OnInit {

  constructor(private ordersService : OrdersService) { }

  completedOrders : Array<Order>;
  notCompletedOrders : Array<Order>;

  updateOrders() {
    this.ordersService.getOrders()
      .subscribe( orders => {
        this.completedOrders = orders.filter(order => order.isCompleted)
        this.notCompletedOrders = orders.filter(order => !order.isCompleted)
      });
  }

  ngOnInit() {
    this.updateOrders();
  }
}
