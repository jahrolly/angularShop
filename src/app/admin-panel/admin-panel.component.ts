import { Component} from '@angular/core';

@Component({
  selector: 'app-admin-panel',
  templateUrl: './admin-panel.component.html',
  styleUrls: ['./admin-panel.component.css']
})
export class AdminPanelComponent {
  showOrders : boolean = false;
  showProducts: boolean = false;
  showDiscounts: boolean = false;

  setShowOrders() {
    this.hideAll();
    this.showOrders = true;
  }

  setShowProducts() {
    this.hideAll();
    this.showProducts = true;
  }

  setShowDiscounts() {
    this.hideAll();
    this.showDiscounts = true;
  }

  private hideAll() {
    this.showOrders = false;
    this.showProducts = false;
    this.showDiscounts = false;
  }
}
