import { Component, OnInit } from '@angular/core';
import {Product} from "../../model/Product";
import {ProductsService} from "../../services/products.service";
import {Discount} from "../../model/Discount";

@Component({
  selector: 'app-discount-component',
  templateUrl: './discount-component.component.html',
  styleUrls: ['./discount-component.component.css']
})
export class DiscountComponentComponent implements OnInit {
  constructor(private productsService: ProductsService) {
  }

  ngOnInit(): void {
    this.getAllProducts();
    this.resetDiscountObject();
  }

  allProducts: Array<Product>;
  discount : Discount;

  private resetDiscountObject() {
    this.discount = new Discount([], 0, 0);
  }

  private getAllProducts() {
    this.productsService.getAllProducts().subscribe(products => {
      this.allProducts = products
    });
  }

  togleProduct(id: string) {
    if (!this.discount.ids.includes(id)) {
      this.discount.ids.push(id)
    }
    else {
      this.discount.ids = this.discount.ids.filter(productId => productId!= id);
    }
  }

  sendDiscountOffer() {
    this.productsService.addDiscount(this.discount);
    console.log(JSON.stringify(this.discount))
    this.resetDiscountObject();
    this.getAllProducts();
  }
}
