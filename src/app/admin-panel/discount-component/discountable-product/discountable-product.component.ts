import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Product} from "../../../model/Product";
import {ProductsService} from "../../../services/products.service";

@Component({
  selector: 'app-discountable-product',
  templateUrl: './discountable-product.component.html',
  styleUrls: ['./discountable-product.component.css']
})

export class DiscountableProductComponent{
  @Input() product : Product;
  @Output() notify : EventEmitter<string> = new EventEmitter<string>();

  toogleElement() {
    this.notify.emit(this.product._id)
  }
}

