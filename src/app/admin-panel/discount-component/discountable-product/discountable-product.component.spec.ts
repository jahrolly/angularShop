import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DiscountableProductComponent } from './discountable-product.component';

describe('DiscountableProductComponent', () => {
  let component: DiscountableProductComponent;
  let fixture: ComponentFixture<DiscountableProductComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DiscountableProductComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DiscountableProductComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
