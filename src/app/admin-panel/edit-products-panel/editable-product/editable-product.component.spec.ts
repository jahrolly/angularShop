import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditableProductComponent } from './editable-product.component';

describe('EditableProductComponent', () => {
  let component: EditableProductComponent;
  let fixture: ComponentFixture<EditableProductComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditableProductComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditableProductComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
