import {Component, Input, OnInit} from '@angular/core';
import {Product} from "../../../model/Product";
import {ProductsService} from "../../../services/products.service";
import {FileUploader} from "ng2-file-upload";
import {urlsService} from "../../../services/urls.service";

@Component({
  selector: 'app-editable-product',
  templateUrl: './editable-product.component.html',
  styleUrls: ['./editable-product.component.css']
})
export class EditableProductComponent {
  @Input() product : Product;
  inputDisabled : boolean = true;
  isDeleted: boolean = false;
  constructor(private productsService : ProductsService, private urls : urlsService) {}

  public uploader:FileUploader = new FileUploader({url: this.urls.getFileUploadUrl()});

  enableInput() {
    this.inputDisabled = false;
  }

  updateProduct() {
    this.inputDisabled = true;
    this.uploader.queue.forEach(item => this.product.images.push(item._file.name));
    this.uploader.uploadAll();
    this.uploader.onCompleteAll = () => {
      this.uploader.clearQueue()
    }
    this.productsService.update(this.product);
  }

  deleteProduct() {
    this.isDeleted = true;
    this.productsService.delete(this.product);
  }
}
