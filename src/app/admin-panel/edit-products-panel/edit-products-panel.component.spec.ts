import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditProductsPanelComponent } from './edit-products-panel.component';

describe('EditProductsPanelComponent', () => {
  let component: EditProductsPanelComponent;
  let fixture: ComponentFixture<EditProductsPanelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditProductsPanelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditProductsPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
