import {Component, Input, OnInit} from '@angular/core';
import {Product, ProductWrapper} from "../../../model/Product";
import {ProductsService} from "../../../services/products.service";
import { FileUploader } from 'ng2-file-upload';
import {urlsService} from "../../../services/urls.service";

@Component({
  selector: 'app-new-product',
  templateUrl: './new-product.component.html',
  styleUrls: ['./new-product.component.css']
})

export class NewProductComponent {
  constructor(private productsService : ProductsService, private urls : urlsService) {}
  productWrapper : ProductWrapper = new ProductWrapper(new Product("", "", 0, 0, "", "", 0));
  public uploader:FileUploader = new FileUploader({url: this.urls.getFileUploadUrl()});

  addProduct() {
    this.uploader.queue.forEach(item => this.productWrapper.product.images.push(item._file.name));
    this.uploader.uploadAll();
    this.uploader.onCompleteAll = () => {
      this.uploader.clearQueue()
    }
    this.productsService.addProduct(this.productWrapper.product);
    this.productWrapper = new ProductWrapper(new Product("", "", 0, 0, "", "", 0));
  }
}
