import {Component, OnInit} from '@angular/core';
import {ProductsService} from "../../services/products.service";
import {Product, ProductWrapper} from "../../model/Product";

@Component({
  selector: 'app-edit-products-panel',
  templateUrl: './edit-products-panel.component.html',
  styleUrls: ['./edit-products-panel.component.css']
})
export class EditProductsPanelComponent implements OnInit {

  allProducts: Array<Product>;
  showAddNewProduct: boolean = false;

  constructor(private productsService: ProductsService) {
  }

  ngOnInit() {
    this.getAllProducts();
  }

  private getAllProducts() {
    this.productsService.getAllProducts().subscribe(products => this.allProducts = products);
  }

  toogleShowNewProduct() {
    this.showAddNewProduct = !this.showAddNewProduct ;
  }
}
