import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {Product} from "../model/Product";
import {ServerNotificationService} from "../services/server-notification.service";
import {AuthorizationService} from "../services/authorization.service";
import {Order} from "../model/Order";
import {UserService} from "../services/user.service";
import {urlsService} from "../services/urls.service";

@Component({
  selector: 'app-home-component',
  templateUrl: './home-component.component.html',
  styleUrls: ['./home-component.component.css']
})
export class HomeComponentComponent {

  checkout: boolean = false;

  constructor(private router: Router, public authorizationService : AuthorizationService,
              private userService : UserService, private urls : urlsService) { }
  login: boolean = false;
  public purchasedProducts: Array<Product>;
  currentView: string = "products";

  productsView: string = "products";
  checkoutView: string = "checkout";
  loginView: string = "login";
  ordersHistory: string = "ordersHistory";
  userOrders: Array<Order> = [];

  goToCheckout(products : Array<Product>) {
    this.currentView = this.checkoutView;
  }

  goToProducts(products : Array<Product>) {
    this.currentView = this.productsView;
  }

  goToLogin() {
    this.currentView = this.loginView;
  }

  goToOrdersHistory() {
    this.userService.getUserOrders(localStorage.getItem(this.urls.username)).subscribe( orders => {
      this.userOrders = orders;
    })
    this.currentView = this.ordersHistory;
  }
}
