import {Injectable} from '@angular/core';
import {urlsService} from "./urls.service";
import {HttpClient} from "@angular/common/http";
import {RawUser} from "../model/User";
import {Router} from "@angular/router";

@Injectable()
export class AuthorizationService {

  constructor(private globalService: urlsService,
              private httpClient: HttpClient,
              private router: Router) {
  }


  login(user: RawUser) {
    this.httpClient.post<any>(this.globalService.checkCredentialsAdmin, user).subscribe(res => {
      let token = res.token;
      localStorage.setItem(this.globalService.authLocalKey, token);
      this.router.navigate(['/admin']);
    },
      err => {
      alert("Niepoprawne dane logowania")
      });
  }

  loginOrdinaryUser(user: RawUser, callback) {
    this.httpClient.post<any>(this.globalService.checkCredentialsUser, user).subscribe(res => {
        console.log(res);
        let token = res.token;
        localStorage.setItem(this.globalService.authLocalKey, token);
        localStorage.setItem(this.globalService.username, user.username);
        callback();
      },
      err => {
        alert("Niepoprawne dane logowania")
      });
  }

  logOut() {
    localStorage.removeItem(this.globalService.authLocalKey);
  }

  isLoggedIn() {
    return localStorage.getItem(this.globalService.authLocalKey) !== null;
  }
}
