import {Injectable} from '@angular/core';
import * as io from 'socket.io-client';
import {BehaviorSubject} from "rxjs/BehaviorSubject";
import {urlsService} from "./urls.service";
import {Observable} from "rxjs/Observable";
import * as Rx from 'rxjs/Rx';

@Injectable()
export class ServerNotificationService {
  socket: any;
  socketConnected$ = new BehaviorSubject<boolean>(false);

  constructor(private urls: urlsService) {
    console.log('Socket initialized');
    this.socket = io('127.0.0.1:1234');
    this.socket.on('connect', () => this.socketConnected$.next(true));
    this.socket.on('disconnect', () => this.socketConnected$.next(false));
    this.socketConnected$.asObservable().subscribe(connected => {
      console.log('Socket connected: ', connected);
    });

    let observable = new Observable(observer => {
      this.socket.on('notification', data => {
        console.log(data)
        console.log("Received notification");
        observer.next(data);
      })
    });
  }

  listen(): Rx.Subject<MessageEvent> {
    let observable = new Observable(observer => {
      this.socket.on('notification', (data) => {
        console.log("Received notification");
        console.log(data);
        observer.next(data);
      });
    });

    let observer = {
      next: (data: Object) => {
        this.socket.emit('message', JSON.stringify(data));
      },
    };
    return Rx.Subject.create(observer, observable);
  }
}
