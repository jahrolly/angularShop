import { TestBed, inject } from '@angular/core/testing';

import { urlsService } from './urls.service';

describe('urlsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [urlsService]
    });
  });

  it('should be created', inject([urlsService], (service: urlsService) => {
    expect(service).toBeTruthy();
  }));
});
