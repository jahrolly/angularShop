import { Injectable } from '@angular/core';
import {Product} from "../model/Product";


@Injectable()
export class urlsService {
public productsURL: string = 'http://localhost:1234/products';
public allProductsURL: string = 'http://localhost:1234/products/all';

private updateProduct: string = 'http://localhost:1234/productsAuth/update';
private addProduct: string = 'http://localhost:1234/productsAuth/add';
private deleteProduct: string = 'http://localhost:1234/productsAuth';
private discountProduct: string = 'http://localhost:1234/productsAuth/discount';

private fileUpload: string = 'http://localhost:1234/imageAuth';

private ordersURL: string = 'http://localhost:1234/orders';
private orderCompleteURL: string = 'http://localhost:1234/orders/';

public checkCredentialsAdmin: string = 'http://localhost:1234/users/admin';
public checkCredentialsUser: string = 'http://localhost:1234/users/user';
public addNewUser: string = 'http://localhost:1234/users/new';
private userOrdersUrl: string = 'http://localhost:1234/users/orders';
private userCartProductUrl: string = 'http://localhost:1234/users/cartProduct';

public authLocalKey: string = "auth_key";
public username: string = "username";

private pathToStatic: string = "http://localhost:1234/static/";

  private getUrlWithToken(url : string) {
    let token = localStorage.getItem(this.authLocalKey) ? '?token=' + localStorage.getItem(this.authLocalKey) : '';
    return `${url}${token}`;
  }

  getOrdersURL() {
    return this.getUrlWithToken(this.ordersURL);
  }

  getCompleteOrderURL() {
    return this.getUrlWithToken(this.orderCompleteURL);
  }

  getUpdateProductUrl() {
    return this.getUrlWithToken(this.updateProduct);
  }

  getDeleteProductUrl(product : Product) {
    return this.getUrlWithToken(this.deleteProduct + '/' + product._id);
  }

  getAddProductUrl() {
    return this.getUrlWithToken(this.addProduct);
  }

  getDiscountProductUrl() {
    return this.getUrlWithToken(this.discountProduct);
  }

  getFileUploadUrl() {
    return this.getUrlWithToken(this.fileUpload);
  }

  getAddNewUserUrl() {
    return this.getUrlWithToken(this.addNewUser);
  }

  getImageLink(fileName: string) {
    return `${this.pathToStatic}${fileName}`;
  }

  getUserOrders(username: string) {
    return this.getUrlWithToken(this.userOrdersUrl + '/' + username);
  }
  getUserCartProducts(productId : string) {
    return this.getUrlWithToken(this.userCartProductUrl + '/' + localStorage.getItem(this.username) + '/' + productId);
  }
  getUserCartProductsAll() {
    return this.getUrlWithToken(this.userCartProductUrl + '/' + localStorage.getItem(this.username));
  }
}
