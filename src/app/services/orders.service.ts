import {Injectable, OnInit} from '@angular/core';
import {Order} from "../model/Order";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs/Observable";
import {urlsService} from "./urls.service";

@Injectable()
export class OrdersService {
  constructor(private http: HttpClient, private globals : urlsService) {
  }

  getOrders(): Observable<Array<Order>> {
    return this.http
        .get<Array<Order>>(this.globals.getOrdersURL());
  }

  markAsCompleted(order : Order) {
    return this.http
      .put<Order>(this.globals.getCompleteOrderURL(), order);
  }
}
