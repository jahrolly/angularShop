import {Injectable, OnInit} from '@angular/core';
import {Product} from "../model/Product";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs/Observable";
import {urlsService} from "./urls.service";
import {ServerNotificationService} from "./server-notification.service";
import {Discount} from "../model/Discount";

@Injectable()
export class ProductsService{

  private availableProducts: Observable<Array<Product>>;

  constructor(private http: HttpClient, private urls: urlsService, private serverNotificationService: ServerNotificationService) {
  }

  getAvailableProducts() : Observable<Array<Product>>{
    return this.http
      .get<Array<Product>>(this.urls.productsURL);
  }

  getAllProducts(): Observable<Array<Product>> {
    return this.http
      .get<Array<Product>>(this.urls.allProductsURL);
  }

  update(product: Product) {
    this.http.put(this.urls.getUpdateProductUrl(), product).subscribe(() => console.log("Product updated"));
  }

  delete(product: Product) {
    console.log(this.urls.getDeleteProductUrl(product));
    this.http.delete(this.urls.getDeleteProductUrl(product)).subscribe(() => console.log("Product deleted"));
  }

  addProduct(product: Product) {
    this.http.post<Product>(this.urls.getAddProductUrl(), product).subscribe(() => console.log("Product added"));
    ;
  }

  addDiscount(discount: Discount) {
    this.http.put<Discount>(this.urls.getDiscountProductUrl(), discount).subscribe(() => console.log("Discounts set"))
  }
}
