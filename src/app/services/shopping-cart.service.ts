import { Injectable } from '@angular/core';
import {Product} from "../model/Product";
import {HttpClient} from "@angular/common/http";
import {urlsService} from "./urls.service";
import {AuthorizationService} from "./authorization.service";

@Injectable()
export class ShoppingCartService {
  constructor(private http: HttpClient, private urls: urlsService, private authService : AuthorizationService) {}
  public purchasedProduct : Array<Product> = [];

  addPurchasedProduct(product: Product): void {
    if (this.authService.isLoggedIn()) {
      this.http.post(this.urls.getUserCartProductsAll(), product).subscribe( () => console.log("Product added to cart"));
    }
    this.purchasedProduct.push(product);
  }

  removePurchasedProducts(productToRemove: Product) {
    if (this.authService.isLoggedIn()) {
      this.http.delete<Product>(this.urls.getUserCartProducts(productToRemove._id)).subscribe(() => console.log("Product removed from cart"));
    }
    this.purchasedProduct = this.purchasedProduct.filter(product => product != productToRemove)
  }

  getNumberOfProducts(): number {
    return this.purchasedProduct.length;
  }

  initLoggedUserShoppingCart() {
    if (this.authService.isLoggedIn()) {
      this.http.get<Array<Product>>(this.urls.getUserCartProductsAll()).subscribe( products =>
        this.purchasedProduct = products)
    }
  }

  getPurchasedProducts() {
    return this.purchasedProduct;
  }

  resetPurchasedProducts() {
    if (this.authService.isLoggedIn()) {
      this.http.delete<Product>(this.urls.getUserCartProductsAll()).subscribe(() => console.log("All products removed from cart"));
    }
    this.purchasedProduct = [];
  }
}
