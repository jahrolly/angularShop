import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {urlsService} from "./urls.service";
import {User} from "../model/User";
import {Order} from "../model/Order";

@Injectable()
export class UserService {
  constructor(private http: HttpClient, private urls: urlsService) { }
  addUser(user: User) {
    this.http.post<User>(this.urls.getAddNewUserUrl(), user).subscribe(() => console.log("User added"));
  }
  addOrderForUser(order : Order, callback) {
    console.log(this.urls.getUserOrders(localStorage.getItem(this.urls.username)));
    console.log(order);
    this.http.put<Order>(this.urls.getUserOrders(localStorage.getItem(this.urls.username)), order).subscribe(callback());
  }

  getUserOrders(username: string) {
    return this.http.get<Array<Order>>(this.urls.getUserOrders(localStorage.getItem(this.urls.username)));
  }
}
