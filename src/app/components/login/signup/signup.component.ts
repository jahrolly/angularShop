import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {User} from "../../../model/User";
import {UserService} from "../../../services/user.service";

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  constructor(private userService : UserService) { }
  @Output() notify : EventEmitter<string> = new EventEmitter<string>();

  user : User = new User("", "", "", "", "", "", 0);

  ngOnInit() {
  }

  sendUser() {
    this.userService.addUser(this.user);
    this.notify.emit("User registered")
  }
}
