import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from "@angular/forms";
import {AuthorizationService} from "../../services/authorization.service";
import {RawUser} from "../../model/User";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;

  constructor(private formBuilder: FormBuilder,
              private authorizationService: AuthorizationService) {
  }

  ngOnInit() {
    this.buildForm();
  }

  login() {
    let user = new RawUser(this.loginForm.value.login, this.loginForm.value.password);
    this.authorizationService.login(user);
  }

  private buildForm() {
    this.loginForm = this.formBuilder.group({
      login: '',
      password: ''
    });
  }
}
