import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {UserService} from "../../services/user.service";
import {Order} from "../../model/Order";

@Component({
  selector: 'app-orders-history',
  templateUrl: './orders-history.component.html',
  styleUrls: ['./orders-history.component.css']
})
export class OrdersHistoryComponent {
  @Input() orders : Array<Order>;
  @Output() notify : EventEmitter<any> = new EventEmitter();
  backToProducts() {
    this.notify.emit("products")
  }
}
