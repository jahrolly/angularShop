import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {AuthorizationService} from "../../services/authorization.service";

@Component({
  selector: 'app-login-header',
  templateUrl: './login-header.component.html',
  styleUrls: ['./login-header.component.css']
})
export class LoginHeaderComponent {
  constructor(public authorizationService : AuthorizationService) {}
  @Output() notify : EventEmitter<string> = new EventEmitter<string>();
  goToLogin() {
    this.notify.emit();
  }
}
