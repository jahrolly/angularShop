import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup} from "@angular/forms";
import {AuthorizationService} from "../../services/authorization.service";
import {RawUser} from "../../model/User";
import {ShoppingCartService} from "../../services/shopping-cart.service";

@Component({
  selector: 'app-userlogin',
  templateUrl: './userlogin.component.html',
  styleUrls: ['./userlogin.component.css']
})
export class UserloginComponent implements OnInit {
  loginForm: FormGroup;

  constructor(private formBuilder: FormBuilder,
              private shoppingCartService : ShoppingCartService,
              private authorizationService: AuthorizationService) {
  }

  @Output() notify : EventEmitter<any> = new EventEmitter<any>();
  userRegistration : boolean = false;

  ngOnInit() {
    this.buildForm();
  }

  login() {
    let user = new RawUser(this.loginForm.value.login, this.loginForm.value.password);
    this.authorizationService.loginOrdinaryUser(user, ()=> {
      this.shoppingCartService.initLoggedUserShoppingCart();
      this.goBack()
    });
  }

  private buildForm() {
    this.loginForm = this.formBuilder.group({
      login: '',
      password: ''
    });
  }

  goBack() {
    this.notify.emit();
  }

  toogleUserRegistration() {
    this.userRegistration = !this.userRegistration;
  }
}

