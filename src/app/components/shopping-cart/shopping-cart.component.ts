import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {ShoppingCartService} from "../../services/shopping-cart.service";
import {Product} from "../../model/Product";


@Component({
  selector: 'app-shopping-cart',
  templateUrl: './shopping-cart.component.html',
  styleUrls: ['./shopping-cart.component.css']
})

export class ShoppingCartComponent {
  constructor(private shoppingCartService: ShoppingCartService) { }
  @Output() notify : EventEmitter<any> = new EventEmitter();

  getNumberOfPurchased(): number {
    return this.shoppingCartService.getNumberOfProducts();
  }

  toCheckout() {
    this.notify.emit(this.shoppingCartService.getPurchasedProducts())
  }
}
