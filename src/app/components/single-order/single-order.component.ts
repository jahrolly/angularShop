import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Product} from "../../model/Product";
import {ProductsService} from "../../services/products.service";
import {Order} from "../../model/Order";
import {OrdersService} from "../../services/orders.service";

@Component({
  selector: 'app-single-order',
  templateUrl: './single-order.component.html',
  styleUrls: ['./single-order.component.css']
})
export class SingleOrderComponent implements OnInit {
  @Input() order: Order;
  products : Array<Product>;
  showDetails: boolean = false;
  totalAmount: Number;

  constructor(private productsService: ProductsService) {
  }

  ngOnInit(): void {
    this.getPurchasedProducts();
  }

  getPurchasedProducts() {
    this.productsService.getAllProducts().subscribe(products => {
      this.products = products.filter(product => this.order.orderedProductsIds.includes(product._id));
      this.totalAmount = this.getTotalAmount();
    });
  }

  getTotalAmount() {
    return (this.products==null) || !this.products.length ? 0 : this.products.map(product => {
      return product.price
    }).reduce((a, b) => {
      return a + b;
    })
  }
}
