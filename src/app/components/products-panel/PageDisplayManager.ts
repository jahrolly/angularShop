import {Product} from "../../model/Product";

export class PageDisplayManager {
  public productsPerPage: Map<Number, Array<Product>>;
  numberOfItemsPerPage: number = 3;

  constructor() {
    this.productsPerPage = new Map();
  }

  updateProductsPerPage(products: Array<Product>): void {
    this.productsPerPage.clear();
    let page: number = 1;
    products.forEach((item, index) => {
      if (this.productsPerPage.has(page))
        this.productsPerPage.get(page).push(item);
      else
        this.productsPerPage.set(page, [item]);
      if ((index + 1) % this.numberOfItemsPerPage == 0) {
        page++;
      }
    })
  }

  getPages(): Array<Number> {
    let pagesMap : Array<Number> = [];
    this.productsPerPage.forEach((value: Array<Product>, key: number) => {
      pagesMap.push(key);
    });
    return pagesMap;
  }

  getProductsForPage(page: number): Array<Product> {
    return this.productsPerPage.get(page);
  }
}
