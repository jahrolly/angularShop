import { Component, OnInit } from '@angular/core';
import {Product} from "../../model/Product";
import {ProductsService} from "../../services/products.service";
import {PageDisplayManager} from "./PageDisplayManager";
import {ShoppingCartService} from "../../services/shopping-cart.service";
import {Observable} from "rxjs/Observable";
import "rxjs/add/observable/from";
import {Filter} from "../../model/Filter";
import {ServerNotificationService} from "../../services/server-notification.service";

@Component({
  selector: 'app-products-panel',
  templateUrl: './products-panel.component.html',
  styleUrls: ['./products-panel.component.css']
})
export class ProductsPanelComponent implements OnInit {
  products: Array<Product>;
  pageDisplayManager: PageDisplayManager;
  currentPage: Number = 1;
  filter : Filter;

  constructor(private productsService: ProductsService, private shoppingCartService: ShoppingCartService, private serverNotificationService: ServerNotificationService) {
    this.pageDisplayManager = new PageDisplayManager();
    this.filter = new Filter("", "", 0, 0);
  }

  ngOnInit(): void {
    this.getProducts();
    this.serverNotificationService.listen().subscribe( notifictation => {
      this.getProducts();
      this.getFilteredProducts();
    })
  }

  getProducts(): void {
      this.productsService.getAvailableProducts()
        .subscribe(
          products => this.products = products,
          error => console.log(error),
          () => this.pageDisplayManager.updateProductsPerPage(this.products)
        );
  }

  private getFilteredProducts() {
    this.productsService.getAvailableProducts()
      .subscribe(
        products => this.products =
          products.filter(product => (this.filter.category== "" || product.category == this.filter.category)
          && ((this.filter.priceFrom == 0 && this.filter.priceTo == 0) || (product.price > this.filter.priceFrom && product.price < this.filter.priceTo))
          && (this.filter.partOfName == "" || product.name.toLowerCase().indexOf(this.filter.partOfName) !== -1)),
        error => console.log(error),
        () => this.pageDisplayManager.updateProductsPerPage(this.products)
      );
    this.pageDisplayManager.updateProductsPerPage(this.products);
    this.currentPage = 1;
  }

  handleCategoryChoice(filter: Filter): void {
    console.log("invoked")
    console.log(filter);
     this.filter = filter;
     this.getFilteredProducts();
  }

  handlePageChoice(page: Number) {
    this.currentPage = page;
  }

  handlePurchase(product: Product) {
    this.shoppingCartService.addPurchasedProduct(product);
  }
}
