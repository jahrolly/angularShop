import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.css']
})
export class PaginationComponent{
  @Input() pages: Array<Number>;
  @Input() activePage: Number;
  @Output() notify : EventEmitter<Number> = new EventEmitter<Number>();

  handlePageClicked(page: Number) {
    this.activePage = page;
    this.notify.emit(page);
  }

  isActive(page : Number) : boolean{
    return page==this.activePage;
  }
}
