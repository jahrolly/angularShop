import {Component, EventEmitter, Input, Output} from '@angular/core';
import {Product} from "../../../model/Product";
import {urlsService} from "../../../services/urls.service";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {ModalInfoComponent} from "./modal-info/modal-info.component";

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent{
  constructor(private urls : urlsService, private modalService: NgbModal) {}
  @Input() product: Product;
  @Output() notify : EventEmitter<Product> = new EventEmitter<Product>();

  handlePurchase() {
    this.notify.emit(this.product);
  }

  isDiscounted() {
    return this.product.DiscountedAmount != 0;
  }

  getImageUrl() {
    if (!this.product.images.length)
      return ""
    return this.urls.getImageLink(this.product.images[0]);
  }


    open(content) {
    const modalRef = this.modalService.open(ModalInfoComponent);
    let images : string[] = this.product.images.map(image => this.urls.getImageLink(image));
    console.log(images);
    modalRef.componentInstance.imagesUrls = images;
  }
}
