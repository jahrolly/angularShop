import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {ProductsService} from "../../../services/products.service";
import {Filter} from "../../../model/Filter";
import {ServerNotificationService} from "../../../services/server-notification.service";


@Component({
  selector: 'app-filters',
  templateUrl: './filters.component.html',
  styleUrls: ['./filters.component.css'],
  providers: [ProductsService]
})
export class FiltersComponent implements OnInit {
  categories: Set<String>;

  @Output() notify : EventEmitter<Filter> = new EventEmitter<Filter>();
  filter : Filter = new Filter("", "", 0, 0);

  constructor(private productsService: ProductsService, private serverNotificationService : ServerNotificationService) { }

  ngOnInit() {
    this.getProductsCategories();
    this.serverNotificationService.listen().subscribe( notifictation => {
      this.getProductsCategories();
    })
  }



  private getProductsCategories() {
    this.productsService.getAvailableProducts()
      .subscribe(products => {
        this.categories = new Set(products.map(product => product.category));
      });
  }

  handleCategoryChoice(category: string) {
    this.filter.category = category;
    this.notify.emit(this.filter);
  }

  handlePriceRange(value) {
    this.filter.priceFrom = value.priceFrom;
    this.filter.priceTo = value.priceTo;
    this.notify.emit(this.filter);
  }

  handleNameSearch(partOfName: string) {
    console.log(partOfName);
    this.filter.partOfName = partOfName;
    this.notify.emit(this.filter);
  }

  resetFilters() {
    this.filter = new Filter("", "", 0, 0);
    this.notify.emit(this.filter);
  }
}
