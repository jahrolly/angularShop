import {Component, EventEmitter, Output} from '@angular/core';

@Component({
  selector: 'app-name-filter',
  templateUrl: './name-filter.component.html',
  styleUrls: ['./name-filter.component.css']
})
export class NameFilterComponent {

  @Output() notify : EventEmitter<String> = new EventEmitter<String>();

  constructor() { }
  public partOfName : string = "";

  updateFilter() {
    this.notify.emit(this.partOfName.toLowerCase());
  }
}
