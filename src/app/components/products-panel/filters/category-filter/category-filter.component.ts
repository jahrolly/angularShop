import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';

@Component({
  selector: 'app-category-filter',
  templateUrl: './category-filter.component.html',
  styleUrls: ['./category-filter.component.css']
})

export class CategoryFilterComponent{
  @Input() category: String;
  @Output() notify : EventEmitter<String> = new EventEmitter<String>();

  handleClick() {
    this.notify.emit(this.category);
  }
}
