import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Product} from "../../model/Product";
import {Order} from "../../model/Order";
import {ShoppingCartService} from "../../services/shopping-cart.service";
import {HttpClient} from "@angular/common/http";
import {urlsService} from "../../services/urls.service";
import {UserService} from "../../services/user.service";


@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.css']
})
export class CheckoutComponent implements OnInit{
  ngOnInit(): void {
    this.shoppingCartService.initLoggedUserShoppingCart();
  }

  constructor(private shoppingCartService: ShoppingCartService, private http: HttpClient, private urls : urlsService,
              private userService : UserService) { }
  @Output() notify : EventEmitter<any> = new EventEmitter();
  order : Order = new Order("", "", "", "", null);



  public form: boolean = false;

  getTotalPrice() {
    return this.shoppingCartService.getPurchasedProducts().reduce((sum,product) => sum + product.price, 0);
  }

  getPurchasedProducts() {
    return this.shoppingCartService.getPurchasedProducts();
  }

  goToForm() {
    this.form = true;
  }

  backToProducts() {
    this.notify.emit();
  }

  removeProduct(productToRemove: Product) {
    this.shoppingCartService.removePurchasedProducts(productToRemove);
  }

  sendOrder() {
    this.order.orderedProductsIds = this.getPurchasedProducts().map(product => product._id);
    console.log(JSON.stringify(this.order));
    this.http.post<Order>(this.urls.getOrdersURL(), this.order).subscribe( (order) => {
      this.userService.addOrderForUser(order, () => {
        this.resetShoppingCart()
      });
    });
  }

  private resetShoppingCart() {
    this.shoppingCartService.resetPurchasedProducts();
    this.order = new Order("", "", "", "", null);
    this.backToProducts();
  }
}
