import { Component } from '@angular/core';
import {Product} from "./model/Product";
import {Router} from "@angular/router";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  constructor(private router: Router) { }

  checkout: boolean = false;
  public purchasedProducts: Array<Product>;

  goToCheckout(products : Array<Product>) {
    this.checkout = true;
  }

  goToProducts(products : Array<Product>) {
    this.checkout = false;
  }

  navToAdmin() {
    this.router.navigateByUrl('/admin');
  }
}
